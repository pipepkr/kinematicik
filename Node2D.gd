extends KinematicBody2D

var p1	
var p2
var pre1 = Vector2()
var pre2 = Vector2()
var speed = 300
var gravity = 100
var mov = Vector2()
onready var pan = get_node("Skeleton2D/pierna/pan")
onready var pie = get_node("Skeleton2D/pierna/pan/pie")
var dir = true
var face = true
var jump = 300

# Called when the node enters the scene tree for the first time.
func _ready():
	p1 = points($RayCast)
	p2 = points($RayCast2)
	
func points(ray):
	var col = ray.get_collider()
	if col:
		return ray.get_collision_point()
	return null
func pointsNormal(ray):
	var col = ray.get_collider()
	if col:
		return ray.get_collision_normal()
	return null

func _physics_process(delta):
	mov.x = 0
	if Input.is_action_pressed("ui_accept") and is_on_floor():
		mov.y = -1500
	if Input.is_action_pressed("ui_left"):
		mov.x -= speed
	if Input.is_action_pressed("ui_right"):
		mov.x += speed
	print(mov.x)
	if mov.x == speed:
		dir = true
	elif mov.x == -speed:
		dir = false
		
	if dir != face:
		face = dir
		scale.x *= -1
	
	
	mov.y += gravity
	mov = move_and_slide(mov, Vector2(0, -1))
	updatePoint(delta)
	
func updatePoint(delta):
	if p1 and p2:
		var dispre1 = Vector2(0,-pre1.distance_to(p1))/2
		var dispre2 = Vector2(0,-pre2.distance_to(p2))/2

		pre1 = pre1 + ((p1-pre1+dispre1)*20*delta)
		pre2 = pre2 + ((p2-pre2+dispre2)*20*delta)

		invertIK(get_node("Skeleton2D/pierna") , pre1, pointsNormal($RayCast))
		invertIK(get_node("Skeleton2D/pierna2"), pre2, pointsNormal($RayCast2))
		
		
		var d1 = points($RayCast)
		var d2 = points($RayCast2)
		if d1:
			if abs($Skeleton2D/cadera.global_position.x - p1.x)>50 and d1.distance_to(p2)>50:
				p1 = d1
		if d2:
			if abs($Skeleton2D/cadera.global_position.x - p2.x)>50 and d2.distance_to(p1)>50:
				p2 = d2
	else:
		p1 = points($RayCast)
		p2 = points($RayCast2)
	
func invertIK(pierna,point,normal):
	#point = get_global_mouse_position()
	var pan = pierna.get_node("pan")
	var pie = pierna.get_node("pan/pie")
	
	var c = pierna.global_position.distance_to(pan.global_position)
	var a = pan.global_position.distance_to(pie.global_position)
	
	var b = pierna.global_position.distance_to(point)

	if point.y > pierna.global_position.y+10:
		if b < c+a and b > 0:
			var v = Vector2(0,c)
			pierna.look_at(point)
			var A = angulo(a,b,c)
			if face:
				A = A-pierna.rotation+PI/2
			else:
				A = -A+pierna.rotation-PI/2
			v = v.rotated(A)
			v.x = -v.x
			pierna.look_at(pierna.global_position+v)
			pan.look_at(point)
			if face:
				pie.global_rotation = normal.angle() + PI/2
			else:
				pie.global_rotation = normal.angle()# + 3*PI/2
		"""else:
			pierna.look_at(point)
			pan.look_at(point)"""

func angulo(a,b,c):
	var A = acos(((b*b)+(c*c)-(a*a))/(2*b*c))
	return A
	
